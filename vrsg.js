;(function (window, document, undefined) {

    var ua = navigator.userAgent;
    var doc = document.documentElement;
    var usingMouse = false;
    var usingTouch = false;
    var lastTouch = Date.now();

    // Helpers
    function isStyleSupported(prop, value) {
        // Don't bother with really old browsers
        if ( !isModern() ) {
            return false;
        }

        // If no value is supplied, use "inherit"
        value = arguments.length === 2 ? value : 'inherit';

        // Try the native standard method first
        if ('CSS' in window && 'supports' in window.CSS) {
            return window.CSS.supports(prop, value);
        }
        // Check Opera's native method
        if('supportsCSS' in window){
            return window.supportsCSS(prop, value);
        }

        return testCssProperty(prop, value);
    }

    function testCssProperty(property, value) {
        var element = document.createElement('link');
        var body = document.getElementsByTagName('HEAD')[0];
        var properties = [];

        var upcaseProp = property.replace(/(^|-)([a-z])/g, function(a, b, c){ return c.toUpperCase(); });   properties[property] =property;
        properties['Webkit'+upcaseProp] ='-webkit-'+property;
        properties['Moz'+upcaseProp] ='-moz-'+property;
        properties['ms'+upcaseProp] ='-ms-'+property;

        body.insertBefore(element, null);

        for (var i in properties) {
            if (element.style[i] !== undefined) {
                element.style[i] = value;
            }
        }

        var st = window.getComputedStyle(element, null),
            currentValue = st.getPropertyValue('-webkit-'+property) || st.getPropertyValue('-moz-'+property) || st.getPropertyValue('-ms-'+property) || st.getPropertyValue(property);

        element.parentNode.removeChild(element);
        return currentValue === value;
    }

    function createStyleSheet(){
        // Create the <style> tag
        var style = document.createElement("style");

        // Add a media (and/or media query) here if you'd like!
        // style.setAttribute("media", "screen")
        // style.setAttribute("media", "only screen and (max-width : 1024px)")

        // WebKit hack :(
        style.appendChild(document.createTextNode(""));

        // Add the <style> element to the page
        document.head.appendChild(style);

        return style.sheet;
    }

    function addCSSRule(sheet, selector, rules, index) {
        index = index || 0;
        if("insertRule" in sheet) {
            sheet.insertRule(selector + "{" + rules + "}", index);
        } else if("addRule" in sheet) {
            sheet.addRule(selector, rules, index);
        }
    }

    function isModern() {
        return (document.addEventListener && document.querySelectorAll && window.getComputedStyle);
    }

    function isMobile() {
        return ua.match(/(iPhone|iPad|iPod|Android|BlackBerry|webOS|Windows Phone)/ig) !== null;
    }

    function isIOS() {
        return ua.match(/(iPhone|iPad|iPod)/ig) !== null;
    }

    function isAndroid() {
        return ua.match(/(Android)/ig) !== null;
    }

    function isIOSSafari() {
        return !!ua.match(/safari/i) && !!ua.match(/iphone|ipad|ipod/i) && !ua.match(/chrome|crios|fxios|edgios/i);
    }

    function isEdge() {
        return ua.match(/(Edge)/ig) !== null;
    }

    function isScrolled() {
        return (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0) > 0;
    }

    function isTouch() {
        return ('ontouchstart' in window || navigator.maxTouchPoints > 0 || navigator.msMaxTouchPoints > 0);
    }

    function isAnchor() {
        return document.location.hash !== '';
    }

    // Constructor
    function Vrsg() {
        var self = this;

        // General state test
        this.test('js', true);
        this.test('outline', false);
        this.test('modern', isModern);
        this.test('anchor', isAnchor);
        this.test('touch', isTouch);
        this.test('autoplay', function(){ return !isMobile(); });

        // Css feature tests
        this.test('flexbox', isStyleSupported('display', 'flex'));
        this.test('csstransforms', isStyleSupported('transform-origin', '50% 50%'));
        this.test('csstransitions', isStyleSupported('transition-duration', '1s'));
        this.test('cssanimations', isStyleSupported('animation-duration', '1s'));
        this.test('objectfit', ('objectFit' in document.documentElement.style !== false));

        this.ready(function(){
            self.test('scrolled', isScrolled);
        });

        // User agent tests

        if( isIOS() ) {
            this.sheet = createStyleSheet();
            addCSSRule( this.sheet, '*', 'cursor: pointer');
            this.addClass('is-ios');
        }

        if (isIOSSafari()) {
            this.addClass('ios-safari');
        }

        if (isAndroid()) {
            this.addClass('is-android');
        }

        if ( isEdge() ) {
            this.addClass('is-msedge');
        }

        // Dynamic state tests
        if (document.addEventListener) {
            document.addEventListener('keydown', function(e) {
                if (e.keyCode === 9 || e.which === 9) {
                    self.test('outline', true);
                }
            });
            document.addEventListener('mousedown', function(e) {
                self.test('outline', false);
            });
            document.addEventListener('touchstart', function(e) {
                if(!usingTouch) {
                    usingTouch = true;
                    usingMouse = false;
                    self.addClass('using-touch');
                    self.removeClass('using-mouse');
                }
                lastTouch = Date.now();
            });
            document.addEventListener('touchend', function(e) {
                lastTouch = Date.now();
            });
            document.addEventListener('mousemove', function(e) {
                if(!usingMouse) {
                    if((Date.now() - lastTouch) > 300) {
                        usingTouch = false;
                        usingMouse = true;
                        self.removeClass('using-touch');
                        self.addClass('using-mouse');
                    }
                }
            });
        }

        return this;
    }

    // Prototype
    Vrsg.prototype = {
        getCookie: function(name) {
            name = name.replace(/([.*+?^=!:${}()|[\]\/\\])/g, '\\$1');
            var regex = new RegExp('(?:^|;)\\s?' + name + '=(.*?)(?:;|$)','i'),
                match = document.cookie.match(regex);

            return match && unescape(match[1]);
        },
        deleteCookie: function(name) {
            if (this.getCookie(name)) {
                this.setCookie(name, '', -1);
            }
            return this;
        },
        setCookie: function(name, value, days, path) {
            var expires = '';
            if (days) {
                var d = new Date();
                d.setTime(d.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = ';expires=' + d.toGMTString();
            }
            path = ';path=' + (path || '/');
            document.cookie = name + '=' + value + expires + path;
            return this;
        },
        addClass: function(className, element) {
            element = element || doc;
            if('classList' in element) {
                element.classList.add(className);
            } else if(!this.hasClass(className, element)) {
                element.className += ' ' + className;
            }
            return this;
        },
        hasClass: function (className, element) {
            element = element || doc;
            if('classList' in element) {
                return element.classList.contains(className);
            } else {
                return !!element.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
            }
        },
        removeClass: function(className, element) {
            element = element || doc;
            if('classList' in element) {
                element.classList.remove(className);
            } else {
                element.className = element.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
            }
            return this;
        },
        ready: function(callback) {
            if (document.readyState === 'interactive' || document.readyState === 'complete') {
                callback();
            } else {
                if (document.addEventListener) {
                    document.addEventListener('DOMContentLoaded', callback);
                } else if(document.attachEvent) {
                    document.attachEvent('onreadystatechange', callback);
                }
            }
        },
        test: function(className, featureTest) {
            var result = typeof featureTest === 'function' ? featureTest() : featureTest;
            if( result ) {
                this.removeClass('no-' + className).addClass(className);
            } else {
                this.removeClass(className).addClass('no-' + className);
            }
        }
    };

    window.vrsg = new Vrsg();

})( window, document );
