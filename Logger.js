/*
  DESCRIPTION: A logging/debugging system, gracefully degrading and enhancing the console output
  while allowing us to leave log statements in our code and controlling when and where to output it by configuration.

  USAGE:
  // Require and get a logger instance
  var log = require('core/Logger').getLogger('modules/some/path/YourModule');

  // It works just like your normal console.log (but also tells you from where it was logged).
  log.debug('string', {object}, whatever...);
  log.info('just so you know, I had bread and aioli for lunch');
  log.warn('ooops, no support for transitions here');
  log.error('oh crap, the server is not responding');
  log.fatal('holy shit, the entire internet is gone');

  BUT THERE IS MORE: After deployment, when logging is turned off - we can enable it, set a log level and filter it.

  By request-parameters:
    - yoursite.com/?logEnabled=true
    - yoursite.com/?logLevel=warn
    - yoursite.com/?logEnabled=true&logFilter=swims

  By globally exposed javascript methods in the console (I know, not really allowed):
    - log.logEnabled(true)
    - log.logLevel('warn')
    - log.logFilter('swims')
*/

  // Private properties
  const DEBUG = 'DEBUG';
  const INFO = 'INFO';
  const WARN = 'WARN';
  const ERROR = 'ERROR';
  const FATAL = 'FATAL';

  const levels = {
    DEBUG: 0,
    INFO: 1,
    WARN: 2,
    ERROR: 3,
    FATAL: 4
  };

  const console = window.console || { log() {} };

  let logLevel = DEBUG;
  let logFilter = null;
  let logStarted = new Date().getTime();
  let logEnabled = true;

  // Private methods
  function setLevel(value) {
    const upper = value.toUpperCase();
    if (levels.hasOwnProperty(upper) && logLevel !== upper) {
      logLevel = upper;
    }
  }

  function getLevel() {
    return logLevel;
  }

  function setFilter(value = null) {
    if (value !== null && logFilter !== value) {
      logFilter = value;
    }
  }

  function getFilter() {
    return logFilter;
  }

  function setEnabled(value = null) {
    if (value !== null && logEnabled !== value) {
      logEnabled = value;
    }
  }

  function getEnabled() {
    return logEnabled;
  }

  function passesFilter(value) {
    return !logFilter || value.match(logFilter);
  }

  function passesLevel(value) {
    return levels[value] >= levels[logLevel];
  }

  function formatLevel(value) {
    switch (value) {
      case 'DEBUG': return 'color: #D0B090;';
      case 'INFO': return 'color: #2090FF;';
      case 'WARN': return 'color: #8B8B00;';
      case 'ERROR': return 'color: #660000;';
      case 'FATAL': return 'color: #FF0000;';
      default: return 'color: #000000;';
    }
  }

  function formatTime(ms) {
    if (ms > 1000) {
      return `${Math.floor(ms / 1000)}s ${ms % 1000}ms`;
    }
    return `${ms}ms`;
  }

  function log(name, level, args) {
    if (logEnabled && passesLevel(level) && passesFilter(name)) {
      const systemTime = new Date().getTime() - logStarted;
      const meta = `%c[${level} ${name} ${formatTime(systemTime)}]%c`;
      console.log(meta, formatLevel(level), formatLevel(null), ...args);
    }
  }

  function extractParamFromUrl(value) {
    const regex = new RegExp(`[\\?|\\&]${value}=(\\w+)`, 'g');
    const match = regex.exec(document.location.href);
    return (match && match.length > 1) ? match[1] : null;
  }

  function applySettingsFromUrlParams() {
    // Apply logLevel
    const logLevelFromUrl = extractParamFromUrl('logLevel');
    if (logLevelFromUrl !== null) {
      setLevel(logLevelFromUrl);
    }
    // Apply logFilter
    const logFilterFromUrl = extractParamFromUrl('logFilter');
    if (logFilterFromUrl !== null) {
      setFilter(logFilterFromUrl);
    }
     // Apply logEnabled
    const logEnabledFromUrl = extractParamFromUrl('logEnabled');
    if (logEnabledFromUrl !== null) {
      setEnabled(logEnabledFromUrl);
    }
  }

  function applySettingsFromOptions(options) {
    if ((typeof options.enabled) !== 'undefined') {
      setEnabled(options.enabled);
    }
    if ((typeof options.level) !== 'undefined') {
      setLevel(options.level);
    }
    if ((typeof options.filter) !== 'undefined') {
      setFilter(options.filter);
    }
    if ((typeof options.timestamp) !== 'undefined') {
      logStarted = options.timestamp;
    }
  }

  function exposeGlobalMethods() {
    window.log = window.log || {};

    window.log.logLevel = level => {
      if (typeof level !== 'undefined') {
        setLevel(level);
      }
      return getLevel();
    };

    window.log.logFilter = filter => {
      if (typeof filter !== 'undefined') {
        setFilter(filter);
      }
      return getFilter();
    };

    window.log.logEnabled = enabled => {
      if (typeof enabled !== 'undefined') {
        setEnabled(enabled);
      }
      return getEnabled();
    };
  }

  // Public api for module
  export const Logger = {
    init(options = {}) {
      // Pick up settings from options
      applySettingsFromOptions(options);

      // Pick up settings from url-params
      applySettingsFromUrlParams();

      // Expose global log methods on window object
      exposeGlobalMethods();
    },
    getLogger(name) {
      return {
        debug(...args) {
          log(name, DEBUG, args);
        },
        info(...args) {
          log(name, INFO, args);
        },
        warn(...args) {
          log(name, WARN, args);
        },
        error(...args) {
          log(name, ERROR, args);
        },
        fatal(...args) {
          log(name, FATAL, args);
        },
        line() {
          console.log('');
        },
        clear() {
          if (typeof console.clear === 'function') {
            console.clear();
          }
        },
        table(...args) {
          if (typeof console.table === 'function') {
            console.table(...args);
          }
        }
      };
    },
    setLevel(value) {
      setLevel(value);
    },
    getLevel() {
      return getLevel();
    },
    setFilter(value) {
      setFilter(value);
    },
    getFilter() {
      return getFilter();
    },
    setEnabled(value) {
      setEnabled(value);
    },
    getEnabled() {
      return getEnabled();
    }
  };

  export const getLogger = name => Logger.getLogger(name);

  export default Logger;
